SHELL := /bin/sh

PYTHON_BIN := $(VIRTUAL_ENV)/bin

pycodestyle:
	$(PYTHON_BIN)/pycodestyle currency_convertor/*

pylint:
	find . -iname "*.py" | xargs $(PYTHON_BIN)/pylint

pylinthtml:
	find . -iname "*.py" | xargs $(PYTHON_BIN)/pylint --output-format=html > pylint_report.html

test:
	coverage run --source='.' tests.py && coverage html --fail-under=95 -d ./htmlcov
