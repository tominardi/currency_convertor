"""
2017 - Thomas Durey
"""

VERSION = (0, 0, 1)


def get_version(detailed=True):
    """
    Returns the current version number

    :param detailed: Bool. If True, returns the version on 3 digiets
                     If False, only 2 digits. Default: True
    :returns: Str, version number
    """
    if detailed:
        return '%s.%s.%s' % VERSION
    return '%s.%s' % VERSION[:2]
