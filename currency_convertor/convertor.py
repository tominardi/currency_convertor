import csv


class Rule:
    origin = None
    target = None
    rate = 1
    direction = 'TO'

    def __init__(self, origin, target, rate):
        """
        :param origin: origin: original currency ('EUR', 'USD', etc)
        :param target: final currency
        :param rate: rate
        """
        self.origin = origin
        self.target = target
        self.rate = float(rate)

    def _convert_to(self, amount):
        """
        Convert amount from origin to target
        :param amount: number
        :return: number
        """
        return amount * self.rate

    def _convert_from(self, amount):
        """

        :param amount:
        :return:
        """
        return amount * (1 / self.rate)

    def convert(self, amount):
        if self.direction == 'TO':
            return self._convert_to(amount)
        return self._convert_from(amount)

    def get_other(self, name):
        if name == self.target:
            return self.origin
        return self.target


class Command:

    def __init__(self, origin, amount, target):
        """
        :param origin: original currency ('EUR', 'USD', etc)
        :param amount: Amount to convert, number
        "param target: final currency
        """
        self.rules = []
        self.path = []
        self.origin = origin
        self.amount = float(amount)
        self.target = target

    def add_rule(self, rule):
        self.rules.append(Rule(*rule))

    def _find(self, name, end, current_path=None):
        if not current_path:
            current_path = self.path
        for rule in self.rules:
            if (rule.origin == name or rule.target == name) and rule not in current_path:
                current_path.append(rule)
                other = rule.get_other(name)
                if rule.target == name:
                    rule.direction = 'FROM'
                if other != end:
                    self._find(other, end, current_path)
                break

    def _resolve(self):
        """
        TODO
        resolve the shortest path
        :return:
        """
        self._find(self.origin, self.target)

    def execute(self):
        self._resolve()
        if self.target not in [self.path[-1].origin, self.path[-1].target]:  # no available path
            return 'n/d'
        amount = self.amount
        for rule in self.path:
            amount = rule.convert(amount)
        return round(amount, 2)


class Convertor:
    def __init__(self, rules_file):
        with open(rules_file) as rules:
            data = csv.reader(rules, delimiter=';')
            index = 0
            for line in data:
                if index == 0:
                    self.command = Command(*line)
                else:
                    self.command.add_rule(line)
                index += 1

    def run(self):
        return self.command.execute()
