import os
import sys

try:
    from currency_convertor.convertor import Convertor
except ImportError:
    PROJECT_PATH = os.path.join(os.getcwd(), '..')
    if PROJECT_PATH not in sys.path:
        sys.path.append(PROJECT_PATH)
    from currency_convertor.convertor import Convertor


def main(file=None):
    convertor = Convertor(rules_file=file)
    return convertor.run()


if __name__ == '__main__':
    # We won't use argparse for such a simple program, by now
    if len(sys.argv) > 1:
        print(main(file=sys.argv[1]))
    else:
        print('Veuillez indiquer le chemin du fichier à parser')
