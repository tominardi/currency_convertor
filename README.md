# Currency convertor #

Convert an amount in a primary currency into the equivalent in another currency

### Context ###

From a list of exchange rates, convert the amount in a first currency into another currency.

A definition file is given. The first line contains the source currency, the amount and the target currency.

The other ones contains the exchange rates.

### Usage in command line ###

```
cd currency_convertor
python currency_conversion.py ../examples/test1.csv
```

### Run tests ###

In the project directory

```
coverage run --source='.' tests.py && coverage html --fail-under=95 -d ./htmlcov
```

or

```
make test
```

### Run pylint ###

```
make pylint
```


### Example ###

Given the following definition file :

| EUR | 20  |    USD     |
|-----|-----|------------|
| EUR | CHF | 1.14651    |
| USD | GBP | 0.742190   |
| USD | XPF | 100.714    |
| GBP | XPF | 135.704    |
| XPF | CHF | 0.00960574 |

When the convertor is called

Then the output is 23.70

#### Detail of conversion ####

* EUR to CHF : c1 = 20 x 1.14651
* CHF to XPF : c2 = c1 x (1 / 0.00960574)
* XPF to USD : r = c2 x (1 / 100.714)
