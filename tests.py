import unittest

from currency_convertor import currency_conversion


class TestConversion(unittest.TestCase):

    def test_euro_to_usd(self):
        example_src = 'examples/test1.csv'
        self.assertEqual(currency_conversion.main(file=example_src), 23.70)

    def test_euro_to_chf(self):
        example_src = 'examples/test2.csv'
        self.assertEqual(currency_conversion.main(file=example_src), 45.86)

    def test_xpf_to_usd(self):
        example_src = 'examples/test3.csv'
        self.assertEqual(currency_conversion.main(file=example_src), 1.99)

    def test_no_path_available(self):
        example_src = 'examples/test4.csv'
        self.assertEqual(currency_conversion.main(file=example_src), 'n/d')


if __name__ == '__main__':
    unittest.main()
