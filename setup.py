"""
2017 - Thomas Durey
"""
import os
import sys

from setuptools import setup, find_packages

PROJECT_PATH = os.path.join(os.getcwd(), 'currency_convertor')

if PROJECT_PATH not in sys.path:
    sys.path.append(PROJECT_PATH)

VERSION = __import__('currency_convertor').get_version()

with open('README.md') as readme_file:
    LONG_DESCRIPTION = readme_file.read()

setup(
    name='currency_convertor',
    version=VERSION,
    url='https://bitbucket.org/tominardi/currency_convertor',
    description='Convert value from a currency to another',
    long_description=LONG_DESCRIPTION,
    author='Thomas Durey',
    author_email='tominardi@gmail.com',
    licence='WTFPL',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    platforms=['Linux'],
    scripts=[
        'currency_convertor/currency_conversion.py',
    ],
)
